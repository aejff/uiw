#!/usr/bin/env bash

#默认随机生成UUID
UUID=${UUID:-'随机'}
VMESS_WSPATH='/vmess'
VLESS_WSPATH='/vless'
TROJAN_WSPATH='/trojan'
SS_WSPATH='/shadowsocks'

#哪吒探针监听系统资源
NEZHA_SERVER=''
NEZHA_PORT=''
NEZHA_KEY=''


URL=$(echo $HOSTNAME|cut -d'-' -f1)-$OKTETO_NAMESPACE.$OKTETO_DOMAIN
[ "$UUID" == "随机" ] && for i in 8 4 4 4 12; do u=${u#-}-$(tr -dc 'a-f0-9' </dev/urandom|head -c $i);done && UUID=${u}
sed -i "s#UUID#$UUID#g;s#VMESS_WSPATH#${VMESS_WSPATH}#g;s#VLESS_WSPATH#${VLESS_WSPATH}#g;s#TROJAN_WSPATH#${TROJAN_WSPATH}#g;s#SS_WSPATH#${SS_WSPATH}#g" config.json
sed -i "s#VMESS_WSPATH#${VMESS_WSPATH}#g;s#VLESS_WSPATH#${VLESS_WSPATH}#g;s#TROJAN_WSPATH#${TROJAN_WSPATH}#g;s#SS_WSPATH#${SS_WSPATH}#g" /etc/nginx/nginx.conf

# 设置 nginx 伪装站
rm -rf /usr/share/nginx/*
wget https://gitlab.com/Misaka-blog/xray-paas/-/raw/main/mikutap.zip -O /tmp/mikutap.zip
unzip -o /tmp/mikutap.zip -d /usr/share/nginx/html
rm -f /tmp/mikutap.zip

# 伪装执行文件
RELEASE_RANDOMNESS=$(tr -dc 'A-Za-z0-9' </dev/urandom | head -c 6)
unzip -qo temp.zip xr$(echo ay) geoip.dat geosite.dat &>/dev/null
mv xr$(echo ay) ${RELEASE_RANDOMNESS}
chmod 755 ${RELEASE_RANDOMNESS}


# 如果有设置哪吒探针三个变量,会安装。如果不填或者不全,则不会安装
[ -n "${NEZHA_SERVER}" ] && [ -n "${NEZHA_PORT}" ] && [ -n "${NEZHA_KEY}" ] && wget https://raw.githubusercontent.com/naiba/nezha/master/script/install.sh -O nezha.sh && chmod +x nezha.sh && ./nezha.sh install_agent ${NEZHA_SERVER} ${NEZHA_PORT} ${NEZHA_KEY}

#创建节点信息
vmlink="vmess://$(echo -n "{\"v\":\"2\",\"ps\":\"okteto-vmess\",\"add\":\"$URL\",\"port\":\"443\",\"id\":\"$UUID\",\"aid\":\"0\",\"net\":\"ws\",\"type\":\"none\",\"host\":\"$URL\",\"path\":\"${VMESS_WSPATH}\",\"tls\":\"tls\"}" | base64 -w 0)"
vllink="vless://$UUID@$URL:443?encryption=none&security=tls&type=ws&host=$URL&path=${VlESS_WSPATH}#okteto-vless"
trlink="trojan://$UUID@$URL:443?security=tls&type=ws&host=$URL&path=${TROJAN_WSPATH}#okteto-trojan"
sslink="ss://$(echo -n "aes-256-gcm://aes-256-gcm:$UUID@$URL:443:ws:${TROJAN_WSPATH}:$URL:tls::[]:"|base64 -w0)#"

link="$RANDOM.html"
cat> /usr/share/nginx/html/$link <<-EOF
<html>
<head>
<title>Okteto节点数据</title>
<link rel="icon" type="image/png" sizes="32x32" href="https://cloud.okteto.com/favicon-32x32.png">
<style type="text/css">
body {
	  font-family: Geneva, Arial, Helvetica, san-serif;
    }
div {
	  margin: 0 auto;
	  text-align: left;
      white-space: pre-wrap;
      word-break: break-all;
      max-width: 80%;
	  margin-bottom: 10px;
}
</style>
</head>
<body bgcolor="#FFFFFF" text="#000000">
<div><font color="#009900"><b style="border-left-style: solid;border-left-width: 0px;">VLESS协议链接：</b><a title="Vless" data-turbo-frame="repo-content-turbo-frame" target="_blank" href="https://$URL${VMESS_WSPATH}" style="font-size: 13px;font-style: italic;">点击测试</a></font></div><div>$vllink</div>
<b>-----------------------------------------------------------------------------------------------------------------------------</b>
<div><font color="#009900"><b>VMESS协议链接：</b><a title="Vmess" data-turbo-frame="repo-content-turbo-frame" target="_blank" href="https://$URL${VLESS_WSPATH}" style="font-size: 13px;font-style: italic;">点击测试</a></font></div><div>$vmlink</div>
<b>-----------------------------------------------------------------------------------------------------------------------------</b>
<div><font color="#009900"><b style="font-style: normal;">Trojan协议链接：</b><a title="Trojan" data-turbo-frame="repo-content-turbo-frame" target="_blank" href="https://$URL${TROJAN_WSPATH}" style="font-size: 13px;font-style: italic;">点击测试</a></font></div><div>$trlink</div>
<b>-----------------------------------------------------------------------------------------------------------------------------</b>
<div><font color="#009900"><b style="font-style: normal;">ShadowSocks协议链接：</b><a title="ShadowSocks" data-turbo-frame="repo-content-turbo-frame" target="_blank" href="https://$URL${SS_WSPATH}" style="font-size: 13px;font-style: italic;">点击测试</a></font></div><div>$sslink</div>
<b>-----------------------------------------------------------------------------------------------------------------------------</b>
</body>
</html>
EOF

cat> /tmp/user_agent <<GET
Mozilla/5.0 (PlayStation; PlayStation 5/2.26) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0 Safari/605.1.15
Mozilla/5.0 (Nintendo Switch; WifiWebAuthApplet) AppleWebKit/601.6 (KHTML, like Gecko) NF/4.0.0.5.10 NintendoBrowser/5.1.0.13343
Mozilla/5.0 (Windows NT 10.0; Win64; x64; XBOX_ONE_ED) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.79 Safari/537.36 Edge/14.14393
Mozilla/5.0 (Windows NT 10.0; Win64; x64; Xbox; Xbox One) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2486.0 Safari/537.36 Edge/13.10586
Mozilla/5.0 (Linux; Android 5.1; SHIELD Build/LMY47N; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/53.0.2785.97 Mobile Safari/537.36
GET

nginx
while true; do curl -I -s --user-agent "\"$(sed -n "$[$RANDOM%5+1],1p" /tmp/user_agent)\"" https://$URL >/dev/null 2>&1 && \
echo "================================================================" && \
echo -e "\nShare URL: https://$URL/$link\n" && \
echo "================================================================" && \
echo "$(date +'%Y-%m-%d %H:%M:%S') Keep Run ..."; sleep $[$RANDOM%971+30]; done &

./${RELEASE_RANDOMNESS} -c=config.json >/dev/null 2>&1 &
tail -f
